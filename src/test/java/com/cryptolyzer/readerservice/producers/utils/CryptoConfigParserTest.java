package com.cryptolyzer.readerservice.producers.utils;

import com.cryptolyzer.readerservice.parsers.ApiConfig;
import com.cryptolyzer.readerservice.parsers.CryptoConfigParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import groovy.json.JsonSlurper;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;


public class CryptoConfigParserTest {

	private String PARSE_RESULT = "{https://api.coinmarketcap.com/v1/ticker/bitcoin/=[https://api.coinmarketcap.com/v1/ticker/bitcoin/, name, price_usd, last_updated, https://api.coinmarketcap.com/v1/ticker/litecoin/, name, price_usd, last_updated], stronka_dla_testu=[http://wtf.wtf/wtf, KRYPTO, CENA, TIMESTAMP]}";
	private static String config_1 = "configApi/configApiForTests.json";
	private static String config_2 = "configApi/configApiJackson.json";

	@Test
	public void parse() throws Exception {
		CryptoConfigParser cryptoConfigParser = new CryptoConfigParser();
		ReflectionTestUtils.setField(cryptoConfigParser, "CONFIG_JSON_FIELD", "config");
		Map<String, Set<ApiConfig>> parse = cryptoConfigParser.parseJackson(CryptoConfigParserTest.class.getClassLoader().getResourceAsStream(config_2));
		//assertTrue(parse.toString().equals(PARSE_RESULT));
		System.out.println(parse);
	}

	@Test
	public void parseSingle() throws JsonProcessingException {
		CryptoConfigParser cryptoConfigParser = new CryptoConfigParser();
		ReflectionTestUtils.setField(cryptoConfigParser, "CONFIG_JSON_FIELD", "config");
		Map<String, Set<ApiConfig>> parseResult = cryptoConfigParser.parse(IOUtils.toInputStream("[{\n" +
				"  \"site\":\"coinmarketcap\",\n" +
				"  \"responseType\":\"key-value\",\n" +
				"  \"period\":\"300\",\n" +
				"  \"config\":{\n" +
				"    \"price\":\"price_usd\",\n" +
				"    \"time\":\"last_updated\"\n" +
				"  },\n" +
				"  \"endpoints\":[{\n" +
				"    \"cryptoName\":\"bitcoin\",\n" +
				"    \"pair\":\"USD\",\n" +
				"    \"url\":\"https://api.coinmarketcap.com/v1/ticker/bitcoin/\"\n" +
				"  },{\n" +
				"    \"cryptoName\":\"litecoin\",\n" +
				"    \"pair\":\"USD\",\n" +
				"    \"url\":\"https://api.coinmarketcap.com/v1/ticker/litecoin/\"\n" +
				"    },{\n" +
				"    \"cryptoName\":\"ethereum\",\n" +
				"    \"pair\":\"USD\",\n" +
				"    \"url\":\"https://api.coinmarketcap.com/v1/ticker/ethereum/\"\n" +
				"  }]\n" +
				"},{\n" +
				"  \"site\":\"bittrex\",\n" +
				"  \"responseType\":\"key-value\",\n" +
				"  \"period\":\"300\",\n" +
				"  \"config\":{\n" +
				"    \"price\":\"result.Last\",\n" +
				"    \"time\":\"\"\n" +
				"  },\n" +
				"  \"endpoints\":[{\n" +
				"    \"cryptoName\":\"litecoin\",\n" +
				"    \"pair\":\"bitcoin\",\n" +
				"    \"url\":\"https://bittrex.com/api/v1.1/public/getticker?market=BTC-LTC\"\n" +
				"  }]\n" +
				"},{\n" +
				"  \"site\":\"bitfinex\",\n" +
				"  \"responseType\":\"array\",\n" +
				"  \"period\":\"300\",\n" +
				"  \"config\":{\n" +
				"    \"price\":\"6\",\n" +
				"    \"time\":\"\"\n" +
				"  },\n" +
				"  \"endpoints\":[{\n" +
				"    \"cryptoName\":\"bitcoin\",\n" +
				"    \"pair\":\"USD\",\n" +
				"    \"url\":\"https://api.bitfinex.com/v2/ticker/tBTCUSD\"\n" +
				"  }]\n" +
				"}]", Charset.defaultCharset()));
		ObjectMapper objectMapper = new ObjectMapper();
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(parseResult));
		new JsonSlurper().parseText(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(parseResult));
	}

}