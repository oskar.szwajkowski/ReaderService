//package com.cryptolyzer.readerservice.producers
//
//import com.cryptolyzer.readerservice.kafka.Sender
//import com.cryptolyzer.readerservice.model.StandardEntity
//import groovy.json.JsonParserType
//import groovy.json.JsonSlurper
//import spock.lang.Specification
//
//class TestProducer extends Specification {
//
//    Sender mockSender = Mock()
//    Producer objectUnderTest = new Producer(mockSender)
//
//    def "Produce"() {
//        when:{
//            objectUnderTest.produce()
//        }
//        then:{
//            1 * mockSender.send(_ as StandardEntity)
//        }
//    }
//
//    static void main(String[] args) {
//        def json = new JsonSlurper().parse('''
//                [8693.1,43.59962921,8696.9,46.98784593,251.4,0.0298,8690.4,49452.07273369,8860,8000]
//            '''.bytes)
//    }
//}
