package com.cryptolyzer.readerservice;

import com.cryptolyzer.readerservice.producers.ProducersManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@EnableAspectJAutoProxy
@ImportResource("classpath:application-context.xml")
public class ReaderserviceApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(ReaderserviceApplication.class, args);
		ProducersManager producerManager = context.getBean(ProducersManager.class);
		producerManager.runProducers();
//		Producer producer3 = (Producer) context.getBean("testProducer");

	}

}
