package com.cryptolyzer.readerservice.configuration;

import com.cryptolyzer.readerservice.kafka.MetadataConsumer;
import com.cryptolyzer.readerservice.model.ConfigMetadata;
import com.cryptolyzer.readerservice.model.StandardEntity;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG;

@Configuration
@EnableKafka
public class ConsumerConfig {

	@Value("${kafka.bootstrap-servers}")
	private String bootstrapServers;

//	public void dupa(){
//		KafkaListenerEndpointRegistry registry = new KafkaListenerEndpointRegistry();
//		registry.registerListenerContainer();
//	}

	@Bean
	MetadataConsumer getMetadataConsumer(){
		return new MetadataConsumer();
	}

	@Bean
	KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, ConfigMetadata>>
	kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, ConfigMetadata> factory =
				new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		factory.setConcurrency(2);
		factory.getContainerProperties().setPollTimeout(3000);
		return factory;
	}

	@Bean
	public ConsumerFactory<String, ConfigMetadata> consumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(), new JsonDeserializer<>(ConfigMetadata.class));
	}

	@Bean("kafkaMessageListenerContainer")
	@Scope("prototype")
	@Lazy
	public KafkaMessageListenerContainer<String, StandardEntity> createContainer(ContainerProperties containerProps) {
		Map<String, Object> props = consumerConfigs();
		DefaultKafkaConsumerFactory<String, StandardEntity> cf =
				new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new JsonDeserializer<>(StandardEntity.class));
		return new KafkaMessageListenerContainer<>(cf, containerProps);
	}

	@Bean
	public Map<String, Object> consumerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		return props;
	}
}
