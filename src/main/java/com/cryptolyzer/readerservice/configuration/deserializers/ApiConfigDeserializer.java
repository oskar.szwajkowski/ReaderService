package com.cryptolyzer.readerservice.configuration.deserializers;

import com.cryptolyzer.readerservice.parsers.ApiConfig;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class ApiConfigDeserializer extends StdDeserializer<ApiConfig> {

	public ApiConfigDeserializer(){
		this(null);
	}

	public ApiConfigDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public ApiConfig deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		JsonNode node = jsonParser.getCodec().readTree(jsonParser);
		return ApiConfig.createConfig(
				node.get("site").asText(),
				node.get("cryptoName").asText(),
				node.get("url").asText(),
				node.get("price").asText(),
				node.get("updateTime").asText(),
				node.get("pair").asText(),
				node.get("interval").asInt(),
				ApiConfig.ResponseType.fromString(
						node.get("responseType").asText()
				)
		);
	}
}
