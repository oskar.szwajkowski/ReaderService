package com.cryptolyzer.readerservice.configuration;

import com.cryptolyzer.readerservice.kafka.Sender;
import com.cryptolyzer.readerservice.model.ConfigMetadata;
import com.cryptolyzer.readerservice.model.StandardEntity;
import org.apache.kafka.clients.Metadata;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource(value = "classpath:reader.properties")
public class SenderConfig {

	@Value("${kafka.bootstrap-servers}")
	private String bootstrapServers;
	@Value("${kafka.topic.json}")
	private String standardTopic;
	@Value("${metadata.topic}")
	private String configTopic;

	@Bean(name = "standardSender")
	@Scope("prototype")
	public Sender<StandardEntity> standardSender(){
		Sender<StandardEntity> sender = new Sender<>();
		sender.setTopic(standardTopic);
		sender.setKafkaTemplate(kafkaTemplate());
		return sender;
	}

	@Bean(name = "metadataSender")
	@Scope("prototype")
	public Sender<ConfigMetadata> metadataSender(){
		Sender<ConfigMetadata> sender = new Sender<>();
		sender.setTopic(configTopic);
		sender.setKafkaTemplate(metadataKafkaTemplate());
		return sender;
	}

	@Bean
	public Map<String, Object> producerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

		return props;
	}

	@Bean
	public ProducerFactory<String, StandardEntity> producerFactory() {
		return new DefaultKafkaProducerFactory<>(producerConfigs());
	}

	@Bean
	public ProducerFactory<String, ConfigMetadata> metadataProducerFactory() {
		return new DefaultKafkaProducerFactory<>(producerConfigs());
	}

	@Bean
	public KafkaTemplate<String, StandardEntity> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}

	public KafkaTemplate<String, ConfigMetadata> metadataKafkaTemplate() {
		return new KafkaTemplate<>(metadataProducerFactory());
	}

	//@Bean
	public KafkaTemplate<String, StandardEntity> kafkaTemplateMock() { return new KafkaTemplateMock<>(producerFactory()); }

	//@Bean
	public KafkaTemplate<String, ConfigMetadata> metadataKafkaTemplateMock() { return new KafkaTemplateMock<>(metadataProducerFactory()); }

	private class KafkaTemplateMock<T> extends  KafkaTemplate<String, T> {

		public KafkaTemplateMock(ProducerFactory<String, T> producerFactory) {
			super(producerFactory);
		}

		@Override
		public org.springframework.util.concurrent.ListenableFuture<org.springframework.kafka.support.SendResult<String, T>> send(String s, T v){
			System.out.println(s + " " + v);
			return null;
		}
	}

}
