package com.cryptolyzer.readerservice.aspects

import com.cryptolyzer.readerservice.model.StandardEntity
import com.cryptolyzer.readerservice.parsers.ApiConfig
import com.cryptolyzer.readerservice.readers.CryptoApiReader
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component


@Aspect
@Component
public class ConnectionValidationAspect {

    Logger logger = LoggerFactory.getLogger(ConnectionValidationAspect.class)

    @Pointcut("execution(* com.cryptolyzer.readerservice.readers.CryptoApiReader.read(java.io.InputStream))")
    void readFromApi() {}

    @Around("readFromApi()")
    StandardEntity validateResponseStatus(ProceedingJoinPoint joinPoint) throws Throwable {
        ApiConfig config = ((CryptoApiReader)joinPoint.getTarget()).getConfig()
        URLConnection get = new URL(config.url).openConnection()
        int responseStatus = get.getResponseCode()
        if (responseStatus == 200) {
            logger.info("Connection to ${config.url} occured succesfully, going for response body serialization")
            return joinPoint.proceed(get.getInputStream()) as StandardEntity
        }
        else {
            logger.error("Connection to ${config.url} occured with response status of ${responseStatus}")
            throw new ConnectException("Failed to connect to: " + config.url)
        }
    }
}
