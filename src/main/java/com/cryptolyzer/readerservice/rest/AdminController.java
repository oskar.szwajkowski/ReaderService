package com.cryptolyzer.readerservice.rest;

import com.cryptolyzer.readerservice.kafka.DataConsumer;
import com.cryptolyzer.readerservice.parsers.ApiConfig;
import com.cryptolyzer.readerservice.producers.ProducersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/")
//@CrossOrigin(origins = "http://localhost:8085")
public class AdminController {

	private final ProducersManager producersManager;
	private final DataConsumer dataConsumer;

	@Autowired
	public AdminController(ProducersManager producersManager, DataConsumer dataConsumer) {
		this.producersManager = producersManager;
		this.dataConsumer = dataConsumer;
	}


	@RequestMapping(
			method = RequestMethod.GET,
			value = "/producers",
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	public List<String> getProducerNames(){
		return producersManager.getProducerNames();
	}

	@RequestMapping(
			method = RequestMethod.GET,
			value = "/producers/running",
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	public List<String> getRunningProducerNames(){
		return producersManager.getRunningProducers();
	}

	@RequestMapping(
			method = RequestMethod.GET,
			value = "/producers/stopped",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
	)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<String> getStoppedProducerNames(){
		return producersManager.getStoppedProducers();
	}

	@RequestMapping(
			method = RequestMethod.GET,
			value = "/start"
	)

	public Object start(@RequestParam(required = false) String site) {
		if (site == null) {
			producersManager.runProducers();
			return "started all";
		} else {
			producersManager.runProducersFromSite(site);
			return "started all from site: " + site;
		}
	}

	@RequestMapping(
			method = RequestMethod.GET,
			value = "/start/{producerName}"
	)
	public String startSingleProducer(@PathVariable String producerName){
		producersManager.runSingleProducer(producerName);
		return "{\"started\":\"" + producerName + "\"}";
	}

	@RequestMapping(
			method = RequestMethod.GET,
			value = "/stop"
	)

	public Object stop() {
		producersManager.stopProducers();
		return "stopped all";
	}

	@RequestMapping(
			method = RequestMethod.GET,
			value = "/stop/{producerName}"
	)
	public String stopSingleProducer(@PathVariable String producerName){
		producersManager.stopSingleProducer(producerName);
		return "{\"stopped\":\"" + producerName + "\"}";
	}

	@RequestMapping(
			method = RequestMethod.GET,
			value = "/reload"
	)

	public Object reload() throws InterruptedException {
		producersManager.reloadProducers();
		return "reloaded";
	}

	@RequestMapping(
			method = RequestMethod.POST,
			value = "/producers"
	)
	public ResponseEntity addOrReplaceProducer(@RequestBody ApiConfig config){
		producersManager.createOrReplaceProducer(config);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(
			method = RequestMethod.POST,
			value = "/topics/add/{topicName}"
	)
	public ResponseEntity addTopicListener(@PathVariable String topicName){
		dataConsumer.addTopicAndRecreateConsumer(topicName);
		return ResponseEntity.accepted().build();
	}

//	public ProducerResponse getProducerDetails(){
//		new ProducerResponse.builder().isRunning(false).config()
//	}


	public Object isWorking() {

		return null;
	}


	public Object report() {
		return null;
	}
}
