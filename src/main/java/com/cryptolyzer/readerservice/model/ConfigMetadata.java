package com.cryptolyzer.readerservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigMetadata {

	public static final String NEW_TOPIC_ADDED = "New topic has been added to kafka";
	public static final String PRODUCER_STARTED = "Producer started";
	public static final String PRODUCER_STOPPED = "Producer stopped";

	private String configTitle;
	private List<String> messages;

	public ConfigMetadata(){}

	public ConfigMetadata(String configTitle, String configMessage){
		this(configTitle, Collections.singletonList(configMessage));
	}

	public ConfigMetadata(String configTitle, List<String> configMessages){
		this(configTitle);
		this.messages = configMessages;
	}

	public ConfigMetadata(String configTitle){
		this.configTitle = configTitle;
	}

	@Override
	public String toString(){
		return configTitle + ", " + messages.toString();
	}

	public List<String> getMessages(){
		return Collections.unmodifiableList(messages);
	}

	public String getConfigTitle(){
		return configTitle;
	}
}
