package com.cryptolyzer.readerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StandardEntity {

	private static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

	private String price;
	private String lastUpdated;

	@Override
	public String toString() {
		return String.format(
				"[price='%s', time='%s']",
				price, lastUpdated);
	}

	public String formatEntity(){
		return String.format("%s, %s", price, getDate());
	}

	private String getDate(){
		return dateFormat.format(new Date(Long.parseLong(lastUpdated) * (lastUpdated.length() < 11 ? 1000 : 1)));
	}

}
