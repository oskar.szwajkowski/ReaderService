package com.cryptolyzer.readerservice.producers

import com.cryptolyzer.readerservice.kafka.Sender
import com.cryptolyzer.readerservice.model.StandardEntity
import com.cryptolyzer.readerservice.parsers.ApiConfig
import com.cryptolyzer.readerservice.readers.CryptoApiReader

class Producer implements Runnable{

    private Sender<StandardEntity> sender
    private CryptoApiReader apiReader

    Producer(Sender<StandardEntity> sender, CryptoApiReader apiReader){
        this.sender = sender
        this.apiReader = apiReader
    }

    void produce(){
        StandardEntity temp = apiReader.read(null)
        sender.send(temp)
    }

    @Override
    void run() {
        this.produce()
    }

    ApiConfig getConfig(){
        return apiReader.getConfig()
    }

    String getTopic(){
        sender.getTopic()
    }

    @Override
    int hashCode(){
        return config.hashCode()
    }

    @Override
    boolean equals(Object producer){
        if (producer instanceof Producer)
            return (this.config == producer.config)
        return false
    }
}
