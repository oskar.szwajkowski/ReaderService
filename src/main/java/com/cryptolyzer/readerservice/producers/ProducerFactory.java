package com.cryptolyzer.readerservice.producers;

import com.cryptolyzer.readerservice.kafka.Sender;
import com.cryptolyzer.readerservice.model.StandardEntity;
import com.cryptolyzer.readerservice.parsers.ApiConfig;
import com.cryptolyzer.readerservice.producers.config.ConfigManager;
import com.cryptolyzer.readerservice.readers.CryptoApiReader;
import com.cryptolyzer.readerservice.readers.ReaderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component("myProducerFactory")
public class ProducerFactory {

	private ConfigManager configManager;
	private ReaderFactory readerFactory;
	private ApplicationContext context;


	public Map<String, Set<Producer>> makeProducersFromConfigs(){
		return configManager.getConfigEntries().entrySet()
				.stream()
				.map(
						entry -> (Map.Entry<String, Set<Producer>>) new AbstractMap.SimpleEntry<>(
								entry.getKey(), createProducers(entry.getValue())
						)
				)
				.collect(
						Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)
				);
	}

	public Producer addOrReplaceProducerConfigAndReturnNewProducer(ApiConfig config){
		configManager.addOrReplaceConfig(config);
		return createProducer(config);
	}

	private Set<Producer> createProducers(Set<ApiConfig> configs){
		Set<Producer> producers = new HashSet<>();
		configs.forEach( config -> producers.add(createProducer(config)));

		return producers;
	}

	private Producer createProducer(ApiConfig config){
		Sender<StandardEntity> sender = (Sender<StandardEntity>) context.getBean("standardSender");
		sender.setTopic(config.topic);
		CryptoApiReader apiReader = readerFactory.getApiReader(config);

		return new Producer(sender, apiReader);
	}

	public int getCryptoConfigsSize() {
		return configManager.getConfigsSize();
	}

	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	@Autowired
	public void setConfigManager(ConfigManager configManager) {
		this.configManager = configManager;
	}

	@Autowired
	public void setReaderFactory(ReaderFactory readerFactory) {
		this.readerFactory = readerFactory;
	}

}
