package com.cryptolyzer.readerservice.producers;

import com.cryptolyzer.readerservice.kafka.KafkaManager;
import com.cryptolyzer.readerservice.kafka.MetadataProducer;
import com.cryptolyzer.readerservice.model.ConfigMetadata;
import com.cryptolyzer.readerservice.parsers.ApiConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component("producersManager")
public class ProducersManager {

	private ProducerFactory producerFactory;
	private ScheduledExecutorService producerManager;
	private Map<String, Set<Producer>> cryptoDataProducers;
	private Map<String, ProducerThread> producerThreads;
	private KafkaManager kafkaManager;

	private MetadataProducer metadataProducer;

	private Logger logger = LoggerFactory.getLogger(ProducersManager.class);

	public ProducersManager() {
		this.producerThreads = new HashMap<>();
	}

	public synchronized void createOrReplaceProducer(ApiConfig producerConfig){
		String producerTopic = producerConfig.topic;
		logger.info("Adding producer: " + producerTopic);

		if(!producerThreads.containsKey(producerTopic))
			kafkaManager.createTopic(producerTopic);

		deleteProducer(producerTopic);
		
		Producer newProducer = producerFactory.addOrReplaceProducerConfigAndReturnNewProducer(producerConfig);
		addOrReplaceProducer(newProducer);
		logger.info("Added producer: " + producerTopic);
	}

	public synchronized void runSingleProducer(String name){
		Producer producer = getProducerOrNull(name);
		if (isProducerStopped(producer)){
			startSingleProducer(producer);
		}
		logger.info("Started producer: " + name);
	}

	public synchronized void stopSingleProducer(String name){
		Producer producer = getProducerOrNull(name);
		if (!isProducerStopped(producer)){
			producerThreads.get(name).getTask().cancel(false);
			metadataProducer.send(new ConfigMetadata(ConfigMetadata.PRODUCER_STOPPED, producer.getTopic()));
		}
		logger.info("Stopped producer: " + name);
	}

	private Producer getProducerOrNull(String name){
		Producer producer = null;
		ProducerThread producerThread = producerThreads.get(name);
		if(producerThread != null) {
			producer = producerThread.getProducer();
			if (producer == null) {
				logger.info("Cannot find producer with topic : " + name);
			}
		}
		return producer;
	}

	public synchronized void runProducersFromSite(String siteName){
		cryptoDataProducers
				.getOrDefault(siteName, Collections.emptySet())
				.forEach(runSingleProducer());
		logger.info("Started producers associated with site: " + siteName);
	}

	public synchronized void runProducers(){
		cryptoDataProducers.forEach(
				runEachProducerEntry()
		);
		logger.info("Started producers");
	}

	public synchronized void stopProducersFromSite(String siteName){
		cryptoDataProducers
				.getOrDefault(siteName, Collections.emptySet())
				.forEach(stopSingleProducer());
		logger.info("Stopped producers associated with site: " + siteName);
	}

	public synchronized void stopProducers(){
		cryptoDataProducers.forEach(
				stopEachProducerEntry()
		);
		logger.info("Stopped producers");
	}

	private BiConsumer<String, Set<Producer>> runEachProducerEntry(){
		return (key, value) -> value.forEach(
				runSingleProducer()
		);
	}

	private Consumer<Producer> runSingleProducer(){
		return producer -> {
			if (isProducerStopped(producer)) {
				startSingleProducer(producer);
			}
		};
	}

	private BiConsumer<String, Set<Producer>> stopEachProducerEntry(){
		return (key, value) -> value.forEach(
				stopSingleProducer()
		);
	}

	private Consumer<Producer> stopSingleProducer(){
		return producer -> {
			if (!isProducerStopped(producer)) {
				stopSingleProducer(producer.getTopic());
			}
		};
	}

	private boolean isProducerStopped(Producer producer){
		String topic = producer.getTopic();
		return producerThreads.get(topic) == null ||
				!producerThreads.get(topic).isRunning();
	}

	private synchronized void startSingleProducer(Producer producer){
		ApiConfig config = producer.getConfig();
		producerThreads.put(
				producer.getTopic(),
				new ProducerThread(producer, producerManager.scheduleWithFixedDelay(
						producer, 10, config.interval, TimeUnit.SECONDS
		)));
		metadataProducer.send(new ConfigMetadata(ConfigMetadata.PRODUCER_STARTED, producer.getTopic()));
		logger.info("Started producer: " + producer.getTopic());
	}
	
	private synchronized void addOrReplaceProducer(Producer producer){
		cryptoDataProducers.computeIfAbsent(producer.getConfig().site, k -> new HashSet<>());
		cryptoDataProducers.get(producer.getConfig().site).add(producer);
		addProducerThread(producer);
		//should producers be automatically started after adding? probably not
		//startSingleProducer(producer);
	}

	private synchronized void deleteProducer(String producerTopic){
		Producer producer = getProducerOrNull(producerTopic);
		if (producer != null){
			logger.info("Producer "+ producerTopic + "already exist, overrind its config and restarting");
			cryptoDataProducers.get(producer.getConfig().site).remove(producer);
			stopSingleProducer(producer.getTopic());
			producerThreads.remove(producer.getTopic());
			logger.info("Removed producer:" + producer.getTopic());
		}
	}

	public List<String> getProducerNames(){
		return producerThreads.entrySet()
				.stream()
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
	}

	public List<String> getRunningProducers(){
		return producerThreads.entrySet()
				.stream()
				.filter(entry -> !isProducerStopped(entry.getValue().getProducer()))
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
	}

	public List<String> getStoppedProducers(){
		return producerThreads.entrySet()
				.stream()
				.filter(entry -> isProducerStopped(entry.getValue().getProducer()))
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
	}

	@PostConstruct
	public synchronized void reloadProducers() throws InterruptedException {
		logger.info("Reloading producers begin...");
		if (producerManager != null) {
			producerManager.shutdown();
			producerManager.awaitTermination(60, TimeUnit.SECONDS);
		}
		int configSize = producerFactory.getCryptoConfigsSize();
		producerManager = new ScheduledThreadPoolExecutor(configSize);
		cryptoDataProducers = producerFactory.makeProducersFromConfigs();
		populateThreadMap();
		logger.info("Reloaded producers, found " + configSize + "configs");
	}

	private void populateThreadMap(){
		cryptoDataProducers.forEach((key, value) -> value.forEach(
				this::addProducerThread
		));
	}

	private void addProducerThread(Producer producer){
		producerThreads.put(
				producer.getTopic(),
				new ProducerThread(producer, null)
		);
	}

	@Autowired
	@Qualifier("myProducerFactory")
	public void setProducerFactory(ProducerFactory producerFactory) {
		this.producerFactory = producerFactory;
	}

	@Autowired
	public void setKafkaManager(KafkaManager kafkaManager) {
		this.kafkaManager = kafkaManager;
	}

	@Autowired
	public void setMetadataProducer(MetadataProducer metadataProducer) {
		this.metadataProducer = metadataProducer;
	}
}
