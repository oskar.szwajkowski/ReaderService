package com.cryptolyzer.readerservice.producers;

import java.util.concurrent.ScheduledFuture;

public class ProducerThread {

	private Producer producer;
	private ScheduledFuture task;

	public ProducerThread(Producer producer){
		this.producer = producer;
	}

	public ProducerThread(Producer producer, ScheduledFuture task){
		this(producer);
		this.task = task;
	}

	public boolean isRunning(){
		return task != null && !task.isDone();
	}

	public Producer getProducer(){
		return producer;
	}

	public ScheduledFuture getTask() {
		return task;
	}

	public void setTask(ScheduledFuture task) {
		this.task = task;
	}
}
