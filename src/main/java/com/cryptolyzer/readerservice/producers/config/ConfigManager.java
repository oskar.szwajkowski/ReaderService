package com.cryptolyzer.readerservice.producers.config;

import com.cryptolyzer.readerservice.parsers.ApiConfig;
import com.cryptolyzer.readerservice.parsers.CryptoConfigParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;

@Component
@PropertySource(value = "classpath:reader.properties")
public class ConfigManager {

	private static final Logger logger = LoggerFactory.getLogger(ConfigManager.class);
	@Value("${configapi.base.file}")
	private String configApiPath;
	private Map<String,Set<ApiConfig>> cryptoConfigs;
	private Set<String> cryptoTopics;
	private CryptoConfigParser cryptoConfigParser;
	private int configsSize;

	@Autowired
	public ConfigManager (CryptoConfigParser cryptoConfigParser){
		this.cryptoConfigParser = cryptoConfigParser;
	}

	public Map<String, Set<ApiConfig>> getConfigEntries() {
		return Collections.unmodifiableMap(cryptoConfigs);
	}

	@PostConstruct
	public synchronized void loadCryptoConfigs() {
		cryptoConfigs = cryptoConfigParser.parseJackson(new File(configApiPath));
		cryptoTopics = new HashSet<>();
		calculateCryptoConfigsSize();
		populateTopics();
	}

	public void addOrReplaceConfig(ApiConfig config){
		Set<ApiConfig> siteConfig = cryptoConfigs.get(config.site);
		if (siteConfig != null) {
			siteConfig.remove(config);
		} else {
			siteConfig = cryptoConfigs.put(config.site, new HashSet<>());
		}
		siteConfig.add(config);
		cryptoTopics.add(config.topic);
		overwriteConfigFile();
	}

	private void overwriteConfigFile() {
		try {
			cryptoConfigParser.serializeToFile(new File(configApiPath), cryptoConfigs);
		} catch (Exception e) {
			logger.error("Error while serializing config to file: " + configApiPath, e);
		} finally {
			calculateCryptoConfigsSize();
		}
	}

	public int getConfigsSize(){
		return configsSize;
	}

	private void calculateCryptoConfigsSize() {
		final int[] size = {0};
		cryptoConfigs.forEach((key, value) -> size[0] += value.size());
		configsSize = size[0];
	}

	private void populateTopics(){
		cryptoConfigs.forEach((key, value) -> value.forEach(config -> cryptoTopics.add(config.topic)));
	}

	public Set<String> getTopics() {
		return Collections.unmodifiableSet(cryptoTopics);
	}
}
