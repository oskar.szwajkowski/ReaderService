package com.cryptolyzer.readerservice.kafka;

import com.cryptolyzer.readerservice.model.StandardEntity;
import com.cryptolyzer.readerservice.producers.config.ConfigManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataConsumer {

	private ApplicationContext applicationContext;
	private ContainerProperties containerProperties;
	private KafkaMessageListenerContainer<String, StandardEntity> container;
	private Set<String> topics;
	private ConfigManager configManager;

	public DataConsumer(){
		topics = new HashSet<>();
	}

	public void start(){
		container.start();
	}

	public void stop(){
		container.stop();
	}

	public void addTopicAndRecreateConsumer(String topic){
		stop();
		topics.add(topic);
		setUp();
	}

	@PostConstruct
	private void postConstruct(){
		topics.addAll(configManager.getTopics());
		setUp();
	}


	private void setUp(){
		containerProperties = new ContainerProperties(topics.toArray(new String[0]));
		containerProperties.setMessageListener(getMessageListener());
		containerProperties.setGroupId("test");
		container = (KafkaMessageListenerContainer<String, StandardEntity>) applicationContext.getBean("kafkaMessageListenerContainer", containerProperties);
		start();
	}

	private MessageListener<String, StandardEntity> getMessageListener(){
		return data -> System.out.println(" RECEIVED FROM TOPIC: " + data.topic() + ", DATA:" + data.value().formatEntity());
	}

	@Autowired
	public void setApplicationContext(ApplicationContext applicationContext){
		this.applicationContext = applicationContext;
	}

	@Autowired
	public void setConfigManager(ConfigManager configManager){
		this.configManager = configManager;
	}
}
