package com.cryptolyzer.readerservice.kafka;

import com.cryptolyzer.readerservice.model.ConfigMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.messaging.handler.annotation.Payload;

@PropertySource("classpath:reader.properties")
public class MetadataConsumer {

	private static final Logger logger = LoggerFactory.getLogger(MetadataConsumer.class);

	@KafkaListener(id = "MetadataConsumer",
			topicPartitions = { @TopicPartition(topic = "metadata", partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "0"))},
			containerFactory = "kafkaListenerContainerFactory")
	public void listen(@Payload ConfigMetadata data) {
		logger.info(String.format("Received config message: %s,", data.toString()));
	}
}
