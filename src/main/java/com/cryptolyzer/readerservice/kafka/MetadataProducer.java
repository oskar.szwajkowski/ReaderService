package com.cryptolyzer.readerservice.kafka;


import com.cryptolyzer.readerservice.model.ConfigMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:reader.properties")
public class MetadataProducer {

	private Sender<ConfigMetadata> kafkaSender;

	public void send(ConfigMetadata configMetadata){
		kafkaSender.send(configMetadata);
	}

	@Autowired
	public void setKafkaSender(Sender<ConfigMetadata> kafkaSender) {
		this.kafkaSender = kafkaSender;
	}
}
