package com.cryptolyzer.readerservice.kafka;

import com.cryptolyzer.readerservice.model.ConfigMetadata;
import com.cryptolyzer.readerservice.parsers.ApiConfig;
import com.cryptolyzer.readerservice.producers.config.ConfigManager;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.KafkaFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.KafkaException;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cryptolyzer.readerservice.model.ConfigMetadata.NEW_TOPIC_ADDED;

@Component
@PropertySource("classpath:reader.properties")
public class KafkaManager {

	private static final Logger logger = LoggerFactory.getLogger(KafkaManager.class);
	private KafkaAdmin kafkaAdmin;
	private AdminClient adminClient;
	private ConfigManager configManager;
	private MetadataProducer metadataProducer;
	@Value("${kafka.number.of.partitions}")
	private int numPartitions;
	@Value("${kafka.replication.factor}")
	private short replicationFactor;

	public void createTopic(String producerTopic) {
		try {
			if (isTopicNew(producerTopic))
				handleTopicCreation(producerTopic);
			else
				logger.info(String.format("Couldn't add topic %s to kafka, as it already exist", producerTopic));
		} catch (InterruptedException | ExecutionException e) {
			handleTopicCreationException(producerTopic, e);
		}
		logger.info("Added topic: "+producerTopic+" to kafka");
	}

	private void handleTopicCreation(String topic) throws ExecutionException, InterruptedException {
		CreateTopicsResult result = createTopic(new NewTopic(topic, numPartitions, replicationFactor));
		KafkaFuture<Void> topicResult = result.values().get(topic);
		topicResult.get();
		if (topicResult.isCompletedExceptionally())
			logger.warn("Topic " + topic + "completed with exception");
		else
			sendTopicCreationMessage(topic);
	}

	private boolean isTopicNew(String topic) throws ExecutionException, InterruptedException {
		return !getTopicsFromKafka().contains(topic);
	}

	private Set<String> getTopicsFromKafka() throws ExecutionException, InterruptedException {
		return adminClient.listTopics().names().get();
	}

	private void handleTopicCreationException(String producerTopic, Exception e){
		String errorMessage = "Error occured during " + producerTopic + " topic creation";
		logger.error(errorMessage, e);
		throw new KafkaException(errorMessage, e);
	}

	private CreateTopicsResult createTopic(NewTopic topic) {
		return createTopics(Collections.singletonList(topic));
	}

	private CreateTopicsResult createTopics(Collection<NewTopic> topics) {
		return adminClient.createTopics(topics);
	}

	@PostConstruct
	private void setUp() throws ExecutionException, InterruptedException {
		kafkaAdmin.setAutoCreate(false);
		adminClient = AdminClient.create(kafkaAdmin.getConfig());
		createTopicsFromConfig();
	}

	private void createTopicsFromConfig() throws ExecutionException, InterruptedException {
		Set<NewTopic> topicsFromConfig = Stream.of(configManager.getConfigEntries())
				.flatMap(m -> m.values().stream().flatMap(Collection::stream))
				.map(c -> new NewTopic(c.topic, numPartitions, replicationFactor))
				.collect(Collectors.toSet());
		adminClient.createTopics(topicsFromConfig).values().forEach((k, v) -> {
			if (!v.isCompletedExceptionally()){
				metadataProducer.send(new ConfigMetadata(ConfigMetadata.NEW_TOPIC_ADDED, k));
				logger.info("Topic {} created, message sent to kafka", k);
			} else
				logger.error("Topic creation failed");
		});
	}

	@Deprecated
	private void createTopicWithListProvided(String producerTopic, Set<String> existingTopics){
		try {
			if (!existingTopics.contains(producerTopic)) {
				handleTopicCreation(producerTopic);
			}
		} catch (InterruptedException | ExecutionException e) {
			handleTopicCreationException(producerTopic, e);
		}
		logger.info("Added topic: "+producerTopic+" to kafka");
	}

	private void sendTopicCreationMessage(String topic){
		metadataProducer.send(new ConfigMetadata(NEW_TOPIC_ADDED, topic));
	}

	@Autowired
	public void setKafkaAdmin(KafkaAdmin kafkaAdmin) {
		this.kafkaAdmin = kafkaAdmin;
	}

	@Autowired
	public void setConfigManager(ConfigManager configManager) {
		this.configManager = configManager;
	}

	@Autowired
	public void setMetadataProducer(MetadataProducer metadataProducer) {
		this.metadataProducer = metadataProducer;
	}

}
