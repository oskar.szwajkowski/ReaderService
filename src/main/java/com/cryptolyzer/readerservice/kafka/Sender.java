package com.cryptolyzer.readerservice.kafka;

import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.core.KafkaOperations;

@PropertySource(value = "classpath:reader.properties")
public class Sender <T> {

	private String topic;
	private KafkaOperations<String, T> kafkaTemplate;

	public void send(T entity) {
		kafkaTemplate.send(topic, entity);
	}

	public void setTopic(String topic){
		this.topic = topic;
	}

	public String getTopic() {
		return topic;
	}

	public void setKafkaTemplate(KafkaOperations<String, T> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}
}
