package com.cryptolyzer.readerservice.parsers

import com.cryptolyzer.readerservice.configuration.deserializers.ApiConfigDeserializer
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import groovy.transform.CompileStatic

import javax.validation.constraints.NotNull

import static com.fasterxml.jackson.annotation.JsonAutoDetect.*

@CompileStatic
@JsonAutoDetect(
        fieldVisibility = Visibility.NONE,
        setterVisibility = Visibility.NONE,
        getterVisibility = Visibility.NONE,
        isGetterVisibility = Visibility.NONE,
        creatorVisibility = Visibility.NONE
)
@JsonDeserialize(using = ApiConfigDeserializer.class)
class ApiConfig {

    @JsonProperty("site")
    public final String site
    @JsonProperty("cryptoName")
    public final String cryptoName
    @JsonIgnore()
    public final String topic
    @JsonProperty("url")
    public final String url
    @JsonProperty("price")
    public final String price
    @JsonProperty("updateTime")
    public final String updateTime
    @JsonProperty("pair")
    public final String pair
    @JsonProperty("interval")
    public final int interval
    @JsonProperty("responseType")
    public final ResponseType responseType


    private ApiConfig(String site,
                      String cryptoName,
                      String url,
                      String price,
                      String updateTime,
                      String pair,
                      int interval,
                      ResponseType responseType){
        this.site = site
        this.cryptoName = cryptoName
        this.url = url
        this.price = price
        this.updateTime = updateTime
        this.pair = pair
        this.interval = interval
        this.responseType = responseType
        this.topic = createTopic(site, cryptoName, pair)
    }

    static ApiConfig createConfig(@NotNull String site, @NotNull String cryptoName, @NotNull String url,
                                  @NotNull String price, @NotNull String time, @NotNull String pair,
                                  @NotNull int interval, @NotNull ResponseType responseType){
        return new ApiConfig(
                site, cryptoName, url, price, time, pair, interval, responseType
        )
    }

//    private String createTopic(){
//        return this.site + "-" + this.cryptoName + "-" + this.pair
//    }

    private static String createTopic(String site, String cryptoName, String pair){
        return site + "-" + cryptoName + "-" + pair
    }

    enum ResponseType {
        KEY_VALUE("key_value"), ARRAY("array")

        private final String type

        private ResponseType(String type){
            this.type = type
        }

        String getType(){ type }

        static ResponseType fromString(String type){
            for ( ResponseType rt : values()){
                if ( rt.type.equalsIgnoreCase(type) ){
                    return rt
                }
            }
            return null
        }
    }

    @Override
    String toString(){
        return "$url, $price, $updateTime"
    }

    @Override
    int hashCode(){
        site.hashCode() + cryptoName.hashCode()
    }

    @Override
    boolean equals(Object config){
        if (config instanceof ApiConfig)
            return (this.site == ((ApiConfig)config).site && this.cryptoName == ((ApiConfig)config).cryptoName)
        return false
    }
}
