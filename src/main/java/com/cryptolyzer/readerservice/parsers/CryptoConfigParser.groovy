package com.cryptolyzer.readerservice.parsers

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonSlurper
import groovy.json.internal.LazyMap
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component

@PropertySource(value = "classpath:reader.properties")
@Component
class CryptoConfigParser {

    @Value(value = '${config.json.field}')
    private String CONFIG_JSON_FIELD

    @Autowired
    private ObjectMapper objectMapper

    @Deprecated
    Map<String, Set<ApiConfig>> parse(File configFile ){
        parse(new FileInputStream(configFile))
    }

    Map<String, Set<ApiConfig>> parseJackson(File configFile ){
        parseJackson(new FileInputStream(configFile))
    }

    @Deprecated
    Map<String, Set<ApiConfig>> parse(InputStream inputStream ){
        Map<String, Set<ApiConfig>> result = new HashMap<>()
        new JsonSlurper().parse( inputStream ).each {
            result.put(it.site, parseKeyValue(it))
        }
        result
    }

    Map<String, Set<ApiConfig>> parseJackson(InputStream inputStream ){
        Map<String, Set<ApiConfig>> result = new HashMap<>()
        LazyMap parsedJson = new JsonSlurper().parse( inputStream ) as LazyMap
        parsedJson.entrySet().each {
            String siteName = it.getKey()
            Set<ApiConfig> tempSet = new HashSet<>()
            ((Set)it.getValue()).each {
                tempSet.add(parseKeyValueJackson(it as LazyMap))
            }
            result.put(siteName, tempSet)
        }
        result
    }

    @Deprecated
    Set<ApiConfig> parseKeyValue(LazyMap json){
        Set<ApiConfig> tempSet = new HashSet<>()
        ApiConfig.ResponseType responseType = ApiConfig.ResponseType.fromString(json.responseType as String)
        String price = json.get(CONFIG_JSON_FIELD).price
        String time = (json.get(CONFIG_JSON_FIELD).time)
        int interval = Integer.parseInt(json.period as String)
        String siteName = json.site

        json.endpoints.each {
            tempSet.add(
                    ApiConfig.createConfig(
                            siteName, it.cryptoName, it.url, price, time, it.pair, interval, responseType
                    ))
        }
        tempSet
    }

    ApiConfig parseKeyValueJackson(LazyMap json){
        ApiConfig.ResponseType responseType = ApiConfig.ResponseType.fromString(json.responseType as String)
        int interval = Integer.parseInt(json.interval as String)
        ApiConfig config = ApiConfig.createConfig(
                json.site, json.cryptoName, json.url, json.price, json.updateTime, json.pair, interval, responseType
        )
        config
    }

    void serializeToFile(File file, Object configs) throws Exception {
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, configs)
    }
}
