package com.cryptolyzer.readerservice.readers;

import com.cryptolyzer.readerservice.parsers.ApiConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ReaderFactory {

	private ApplicationContext context;

	public CryptoApiReader getApiReader(ApiConfig config){
		CryptoApiReader returned;
		switch ( config.responseType ){
			case KEY_VALUE:
				returned = (CryptoApiReader) context.getBean("keyValueApiReader");
				break;
			case ARRAY:
				returned = (CryptoApiReader) context.getBean("arrayApiReader");
				break;
			default:
				throw new RuntimeException("Couldn't create ApiReader from type: " + config.responseType);
		}
		returned.setConfig(config);
		return returned;
	}

	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
	}
}
