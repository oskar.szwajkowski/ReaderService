package com.cryptolyzer.readerservice.readers

import com.cryptolyzer.readerservice.model.StandardEntity
import com.cryptolyzer.readerservice.parsers.ApiConfig
import groovy.json.JsonSlurper
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component("arrayApiReader")
@Scope("prototype")
class ArrayApiReader implements CryptoApiTrait {

    private ApiConfig config
    private JsonSlurper jsonSlurper

    ArrayApiReader(){
        jsonSlurper = new JsonSlurper()
    }

    ArrayApiReader(ApiConfig config){
        super()
        this.config = config
    }

    @Override
    StandardEntity read(InputStream inputStream) throws IOException {
        def responseJson = jsonSlurper.parse(inputStream)
        def price, lastUpdated

        price = responseJson[Integer.parseInt(config.price)].toString()
        lastUpdated = config.updateTime != "" ? responseJson[Integer.parseInt(config.updateTime)].toString() : String.valueOf(new Date().getTime())

        return new StandardEntity(price, lastUpdated)
    }

    @Override
    ApiConfig getConfig() {
        return config
    }

    @Override
    void setConfig(ApiConfig config) {
        this.config = config
    }
}
