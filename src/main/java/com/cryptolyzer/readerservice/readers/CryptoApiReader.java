package com.cryptolyzer.readerservice.readers;


import com.cryptolyzer.readerservice.model.StandardEntity;
import com.cryptolyzer.readerservice.parsers.ApiConfig;

import java.io.IOException;
import java.io.InputStream;

public interface CryptoApiReader {
	StandardEntity read(InputStream inputStream) throws IOException;
	ApiConfig getConfig();
	void setConfig(ApiConfig config);
}
