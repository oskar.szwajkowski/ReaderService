package com.cryptolyzer.readerservice.readers

import com.cryptolyzer.readerservice.model.StandardEntity
import com.cryptolyzer.readerservice.parsers.ApiConfig
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component("keyValueApiReader")
@Scope("prototype")
class KeyValueApiReader implements CryptoApiTrait {

    private ApiConfig config

    @Autowired
    private JsonSlurper jsonSlurper

    KeyValueApiReader(){}

    KeyValueApiReader(ApiConfig config){
        this.config = config
    }

    @Override
    StandardEntity read(InputStream inputStream) throws IOException {
        def responseJson = jsonSlurper.parse(inputStream)
        def price, lastUpdated

        if (responseJson.getClass().isArray() || responseJson instanceof List<?>) {
            price = parseProperty(responseJson[0], config.price)
            lastUpdated = config.updateTime != "" ? parseProperty(responseJson[0], config.updateTime) : String.valueOf(new Date().getTime())
        } else {
            price = parseProperty(responseJson, config.price)
            lastUpdated = config.updateTime != "" ? parseProperty(responseJson, config.updateTime) : String.valueOf(new Date().getTime())
        }

        return new StandardEntity(price, lastUpdated)
    }

    @Override
    ApiConfig getConfig() {
        return config
    }

    void setConfig(ApiConfig config) {
        this.config = config
    }
}
