package com.cryptolyzer.readerservice.readers

trait CryptoApiTrait implements  CryptoApiReader{
    String parseProperty(Object json, String jsonKey){
        String[] keys = jsonKey.split("\\.")
        keys.each {
            json = json.get(it)
        }
        return json
    }
}